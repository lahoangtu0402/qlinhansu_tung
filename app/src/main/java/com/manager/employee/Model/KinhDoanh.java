package com.manager.employee.Model;

import java.io.Serializable;

public class KinhDoanh extends Employee implements Serializable {

    double doanhThu;

    public double getDoanhThu() {
        return doanhThu;
    }

    public void setDoanhThu(double doanhThu) {
        this.doanhThu = doanhThu;
    }

    public KinhDoanh(String ma, String ten, String sdt, String ngaySinh, byte[] hinh, double luongCB, double doanhThu, int maPB) {
        super(ma, ten, sdt, ngaySinh, hinh, luongCB,maPB);
        this.doanhThu = doanhThu;
    }

    @Override
    public double tinhLuong() {
        return this.getLuongCB() * 0.5 + doanhThu * 0.25;
    }

    @Override
    public String getMoreOption() {
        return doanhThu+"";
    }
}
