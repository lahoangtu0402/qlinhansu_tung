package com.manager.employee.Model;

import java.io.Serializable;

public class NVKhac extends Employee implements Serializable {
    public static double TRO_CAP = 5000000;

    public NVKhac(String ma, String ten, String sdt, String ngaySinh, byte[] hinh, double luongCB, int maPB) {
        super(ma, ten, sdt, ngaySinh, hinh, luongCB,maPB);
    }

    @Override
    public double tinhLuong() {
        return  getLuongCB() + TRO_CAP;
    }

    @Override
    public String getMoreOption() {
        return TRO_CAP+"";
    }
}
