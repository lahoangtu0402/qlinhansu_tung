package com.manager.employee.Model;

import java.io.Serializable;

public abstract class Employee implements Serializable, Comparable<Employee>{
    private  String ma;
    private  String ten;
    private  String sdt;
    private String ngaySinh;
    private  byte[] hinh;
    private  double luongCB;
    private int maPB;

    public Employee() { }

    @Override
    public int compareTo(Employee o) {
        return ten.compareToIgnoreCase(o.ten);
    }

    //    //init
//    public Employee(String ma, String ten, String sdt, String hinh, double luongCB) {
//        this.ma = ma;
//        this.ten = ten;
//        this.sdt = sdt;
//        this.hinh = hinh;
//        this.luongCB = luongCB;
//    }
    public Employee(String ma, String ten, String sdt, String ngaySinh, byte[] hinh, double luongCB, int maPB) {
        this.ma = ma;
        this.ten = ten;
        this.sdt = sdt;
        this.ngaySinh = ngaySinh;
        this.hinh = hinh;
        this.luongCB = luongCB;
        this.maPB = maPB;
    }

    public int getMaPB() {
        return maPB;
    }

    public void setMaPB(int maPB) {
        this.maPB = maPB;
    }

    public String getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(String ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public String getMa() {
        if(ma.contains("NV") || ma.contains("-")){
            ma = ma.replace("NV","");
            ma = ma.replace("-","");
        }
        setMa(ma);
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getSdt() {
        return sdt;
    }

    public void setSdt(String sdt) {
        this.sdt = sdt;
    }

    public byte[] getHinh() {
        return hinh;
    }

    public void setHinh(byte[] hinh) {
        this.hinh = hinh;
    }

    public double getLuongCB() {
        return luongCB;
    }

    public void setLuongCB(double luongCB) {
        this.luongCB = luongCB;
    }

    public abstract double tinhLuong();

    public abstract String getMoreOption();
}
