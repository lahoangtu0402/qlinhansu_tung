package com.manager.employee.Model;

import java.io.Serializable;

public class KyThuat extends Employee implements Serializable {
    private  double troCap;

    public KyThuat(String ma, String ten, String sdt, String ngaySinh, byte[] hinh, double luongCB, double troCap, int maPB) {
        super(ma, ten, sdt, ngaySinh, hinh, luongCB, maPB);
        this.troCap = troCap;
    }

    public double getTroCap() {
        return troCap;
    }

    public void setTroCap(double troCap) {
        this.troCap = troCap;
    }

    @Override
    public double tinhLuong() {
        return  getLuongCB() * 0.75 + troCap;
    }

    @Override
    public String getMoreOption() {
        return troCap+"";
    }
}
