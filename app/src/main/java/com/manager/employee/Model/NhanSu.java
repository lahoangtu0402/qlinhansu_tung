package com.manager.employee.Model;

import java.io.Serializable;

public class NhanSu extends Employee implements Serializable {
    private int slNVQL;

    public int getSlNVQL() {
        return slNVQL;
    }

    public void setSlNVQL(int slNVQL) {
        this.slNVQL = slNVQL;
    }

    public NhanSu(String ma, String ten, String sdt, String ngaySinh, byte[] hinh, double luongCB, int slNVQL, int maPB) {
        super(ma, ten, sdt, ngaySinh, hinh, luongCB,maPB);
        this.slNVQL = slNVQL;
    }

    @Override
    public double tinhLuong() {
        return  getLuongCB() * 0.4 + slNVQL * 0.1 ;
    }

    @Override
    public String getMoreOption() {
        return slNVQL+"";
    }
}
