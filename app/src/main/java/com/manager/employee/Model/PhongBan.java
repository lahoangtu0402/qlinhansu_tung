package com.manager.employee.Model;

import java.io.Serializable;

public class PhongBan implements Serializable {
    private  int maPB;
    private String tenPB;

    public int getMaPB() {
        return maPB;
    }

    public void setMaPB(int maPB) {
        this.maPB = maPB;
    }

    public String getTenPB() {
        return tenPB;
    }

    public void setTenPB(String tenPB) {
        this.tenPB = tenPB;
    }


    public PhongBan(int maPB, String tenPB) {
        this.maPB = maPB;
        this.tenPB = tenPB;
    }

    private boolean isChecked;
    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    @Override
    public String toString() {
        return tenPB;
    }
}
