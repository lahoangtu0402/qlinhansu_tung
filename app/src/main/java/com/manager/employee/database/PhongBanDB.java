package com.manager.employee.database;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.manager.employee.Model.PhongBan;

import java.util.ArrayList;


public class PhongBanDB {

    public static DBHelper dbHelper;
    private SQLiteDatabase db;
    public PhongBanDB(Activity activity) {
        try {
            dbHelper = new DBHelper(activity);
            db = dbHelper.getWritableDatabase();
        }catch (Exception e){}
    }


    public ArrayList<PhongBan> getListPhongBan(){
        ArrayList<PhongBan> result = new ArrayList<>();

        String sql = "SELECT * FROM " + ManagerTable.PhongBanTable.TABLE_NAME +
                " ORDER BY " + ManagerTable.PhongBanTable.CREATE_AT + " DESC ;";
        Cursor cursor = dbHelper.GetData(sql);
        if(cursor!=null) {
            while (cursor.moveToNext()){
                int id = cursor.getInt(0);
                String name = cursor.getString(cursor.getColumnIndex(ManagerTable.PhongBanTable.NAME));
                result.add(new PhongBan(id,name));
            }
        }
        return result;
    }

    public boolean addPhongBan(String tenPB){
        ContentValues recent = new ContentValues();
        recent.put(ManagerTable.PhongBanTable.NAME,tenPB);
        long result = 0;
        try {
            db = dbHelper.getWritableDatabase();
            result = db.insert(ManagerTable.PhongBanTable.TABLE_NAME, null, recent);
            db.close();
        }catch(Exception ex) {
            Log.e("Error DB", ex.getMessage());
        }
        return result > 0;

    }
    public boolean deletePhongBan(int MaPB){
        boolean result = false;
        String sql_where  =  "_id = "+MaPB;

        try {
            db = dbHelper.getWritableDatabase();
            result = db.delete(ManagerTable.PhongBanTable.TABLE_NAME, sql_where,null) > 0;
            db.close();
        }catch(Exception ex) {
            Log.e("Error DB", ex.getMessage());
        }
        return result;
    }
    public boolean updatePhongBan(int MaPB,String tenPB){
        boolean result = false;
        String sql_where  =  "_id = "+MaPB;
        ContentValues contentValues = new ContentValues();
        contentValues.put(ManagerTable.PhongBanTable.NAME,tenPB);
        try {
            db = dbHelper.getWritableDatabase();
            result = db.update(ManagerTable.PhongBanTable.TABLE_NAME, contentValues,sql_where,null) > 0;
            db.close();
        }catch(Exception ex) {
            Log.e("Error DB", ex.getMessage());
        }
        return result;
    }

    public String getTenPB(int maPB) {
        String ten = "";
        Cursor cursor = null;
        try {
            String sql = "SELECT * FROM " + ManagerTable.PhongBanTable.TABLE_NAME +
                    " ORDER BY " + ManagerTable.PhongBanTable.CREATE_AT + " DESC ;";
            //            Cursor cursor = dbHelper.GetData(sql);


            cursor = db.rawQuery("SELECT * FROM " + ManagerTable.PhongBanTable.TABLE_NAME + " WHERE "+  ManagerTable.PhongBanTable._ID +" = ?", new String[] {String.valueOf(maPB)});
            if(cursor.getCount() > 0) {
                cursor.moveToFirst();
                ten = cursor.getString(cursor.getColumnIndex(ManagerTable.PhongBanTable.NAME));
            }
            return ten;
        }finally {
            cursor.close();
        }
    }


}
