package com.manager.employee.database;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.manager.employee.Config.Config;
import com.manager.employee.Model.Employee;
import com.manager.employee.Model.KinhDoanh;
import com.manager.employee.Model.KyThuat;
import com.manager.employee.Model.NVKhac;
import com.manager.employee.Model.NhanSu;

import java.util.ArrayList;


public class NhanVienDB {

    public static DBHelper dbHelper;
    private SQLiteDatabase db;
    public NhanVienDB(Activity activity) {
        try {
            dbHelper = new DBHelper(activity);
            db = dbHelper.getWritableDatabase();
        }catch (Exception e){}
    }



    public ArrayList<Employee> getListNhanVien(int offset ){
        ArrayList<Employee> result = new ArrayList<>();

        String sql = "SELECT * FROM " + ManagerTable.NhanVienTable.TABLE_NAME +
                " ORDER BY " + ManagerTable.NhanVienTable.CREATE_AT + " DESC "+
                " LIMIT "+ Config.LIMIT_RECORD  + " OFFSET "+ offset ;
        Cursor cursor = dbHelper.GetData(sql);

        if(cursor!=null) {
            while (cursor.moveToNext()){
                int maNV = cursor.getInt(0);
                String name = cursor.getString(cursor.getColumnIndex(ManagerTable.NhanVienTable.NAME));
                String sdt = cursor.getString(cursor.getColumnIndex(ManagerTable.NhanVienTable.SDT));
                String ngaysinh = cursor.getString(cursor.getColumnIndex(ManagerTable.NhanVienTable.NGAY_SINH));
                String luongCB = cursor.getString(cursor.getColumnIndex(ManagerTable.NhanVienTable.LUONG_CB));
                byte[] hinh = cursor.getBlob(cursor.getColumnIndex(ManagerTable.NhanVienTable.HINH));
                String option =  cursor.getString(cursor.getColumnIndex(ManagerTable.NhanVienTable.OPTION_MORE));

                int maPB = cursor.getInt(cursor.getColumnIndex(ManagerTable.NhanVienTable.MAPB));
                switch (maPB){
                    case 1://ky thuat
                        result.add(new KyThuat(maNV+"",name,sdt,ngaysinh,hinh,Double.parseDouble(luongCB),Double.parseDouble(option),maPB));
                        break;
                    case 2://nhan su
                        result.add(new NhanSu(maNV+"",name,sdt,ngaysinh,hinh,Double.parseDouble(luongCB), Integer.parseInt(option),maPB));
                        break;
                    case 3://kinh doanh
                        result.add(new KinhDoanh(maNV+"",name,sdt,ngaysinh,hinh,Double.parseDouble(luongCB),Double.parseDouble(option),maPB));
                        break;
                    default:
                        result.add(new NVKhac(maNV+"",name,sdt,ngaysinh,hinh,Double.parseDouble(luongCB),maPB));
                        break;
                }
            }
        }
        return result;
    }
    public ArrayList<Employee> getListNhanVienByMaPB(ArrayList<String> listMaPB,int offset){
        ArrayList<Employee> result = new ArrayList<>();
        String temp = "";
        for (int i = 0 ; i<listMaPB.size() ; i++){
            if(i == listMaPB.size() - 1){
                temp = temp + listMaPB.get(i);
            }else{
                temp = temp + listMaPB.get(i) + " , ";
            }

        }

        String sql = "SELECT * FROM " + ManagerTable.NhanVienTable.TABLE_NAME +
                " WHERE "+ ManagerTable.NhanVienTable.MAPB + " IN ( " + temp + " ) " +
                " ORDER BY " + ManagerTable.NhanVienTable.CREATE_AT + " DESC "+
                " LIMIT "+ Config.LIMIT_RECORD  + " OFFSET "+ offset;
        Cursor cursor = dbHelper.GetData(sql);

        if(cursor!=null) {
            while (cursor.moveToNext()){
                int maNV = cursor.getInt(0);
                String name = cursor.getString(cursor.getColumnIndex(ManagerTable.NhanVienTable.NAME));
                String sdt = cursor.getString(cursor.getColumnIndex(ManagerTable.NhanVienTable.SDT));
                String ngaysinh = cursor.getString(cursor.getColumnIndex(ManagerTable.NhanVienTable.NGAY_SINH));
                String luongCB = cursor.getString(cursor.getColumnIndex(ManagerTable.NhanVienTable.LUONG_CB));
                byte[] hinh = cursor.getBlob(cursor.getColumnIndex(ManagerTable.NhanVienTable.HINH));
                String option =  cursor.getString(cursor.getColumnIndex(ManagerTable.NhanVienTable.OPTION_MORE));

                int maPB = cursor.getInt(cursor.getColumnIndex(ManagerTable.NhanVienTable.MAPB));
                switch (maPB){
                    case 1://ky thuat
                        result.add(new KyThuat(maNV+"",name,sdt,ngaysinh,hinh,Double.parseDouble(luongCB),Double.parseDouble(option),maPB));
                        break;
                    case 2://nhan su
                        result.add(new NhanSu(maNV+"",name,sdt,ngaysinh,hinh,Double.parseDouble(luongCB), Integer.parseInt(option),maPB));
                        break;
                    case 3://kinh doanh
                        result.add(new KinhDoanh(maNV+"",name,sdt,ngaysinh,hinh,Double.parseDouble(luongCB),Double.parseDouble(option),maPB));
                        break;
                    default:
                        result.add(new NVKhac(maNV+"",name,sdt,ngaysinh,hinh,Double.parseDouble(luongCB),maPB));
                        break;
                }
            }
        }
        return result;
    }
    public int getIDNew(){
        String sql = "SELECT * FROM " + ManagerTable.NhanVienTable.TABLE_NAME +
                " ORDER BY " + ManagerTable.NhanVienTable._ID + " DESC LIMIT 1";
        Cursor cursor = dbHelper.GetData(sql);

        if(cursor!=null) {
            while (cursor.moveToNext()){
                int maNV = cursor.getInt(cursor.getColumnIndex(ManagerTable.NhanVienTable._ID));
                return maNV + 1;
            }
        }

        return 1;
    }

    public boolean addNhanVien(Employee nhanVien, int maPB){
        ContentValues recent = new ContentValues();
        recent.put(ManagerTable.NhanVienTable.NAME,nhanVien.getTen());
        recent.put(ManagerTable.NhanVienTable.SDT,nhanVien.getSdt());
        recent.put(ManagerTable.NhanVienTable.NGAY_SINH,nhanVien.getNgaySinh());
        recent.put(ManagerTable.NhanVienTable.HINH,nhanVien.getHinh());
        recent.put(ManagerTable.NhanVienTable.LUONG_CB,nhanVien.getLuongCB()+"");

        recent.put(ManagerTable.NhanVienTable.MAPB,maPB);


        if(nhanVien instanceof KinhDoanh){
            recent.put(ManagerTable.NhanVienTable.OPTION_MORE,((KinhDoanh) nhanVien).getDoanhThu());
        }else
        if(nhanVien instanceof KyThuat){
            recent.put(ManagerTable.NhanVienTable.OPTION_MORE,((KyThuat) nhanVien).getTroCap());
        }else
        if(nhanVien instanceof NhanSu){
            recent.put(ManagerTable.NhanVienTable.OPTION_MORE,((NhanSu) nhanVien).getSlNVQL());
        }else
            recent.put(ManagerTable.NhanVienTable.OPTION_MORE, NVKhac.TRO_CAP + "");



        long result = 0;
        try {
            db = dbHelper.getWritableDatabase();
            result = db.insert(ManagerTable.NhanVienTable.TABLE_NAME, null, recent);
            db.close();
        }catch(Exception ex) {
            Log.e("Error DB", ex.getMessage());
        }
        return result > 0;

    }

    public boolean updateNhanVien(Employee nhanVien){
        ContentValues recent = new ContentValues();
        recent.put(ManagerTable.NhanVienTable.NAME,nhanVien.getTen());
        recent.put(ManagerTable.NhanVienTable.SDT,nhanVien.getSdt());
        recent.put(ManagerTable.NhanVienTable.NGAY_SINH,nhanVien.getNgaySinh());
        recent.put(ManagerTable.NhanVienTable.HINH,nhanVien.getHinh());
        recent.put(ManagerTable.NhanVienTable.LUONG_CB,nhanVien.getLuongCB()+"");

        recent.put(ManagerTable.NhanVienTable.MAPB,nhanVien.getMaPB());


        if(nhanVien instanceof KinhDoanh){
            recent.put(ManagerTable.NhanVienTable.OPTION_MORE,((KinhDoanh) nhanVien).getDoanhThu());
        }else
        if(nhanVien instanceof KyThuat){
            recent.put(ManagerTable.NhanVienTable.OPTION_MORE,((KyThuat) nhanVien).getTroCap());
        }else
        if(nhanVien instanceof NhanSu){
            recent.put(ManagerTable.NhanVienTable.OPTION_MORE,((NhanSu) nhanVien).getSlNVQL());
        }else{
            recent.put(ManagerTable.NhanVienTable.OPTION_MORE, NVKhac.TRO_CAP + "");
        }


        long result = 0;
        try {
            db = dbHelper.getWritableDatabase();
            result  = db.update(ManagerTable.NhanVienTable.TABLE_NAME,recent,ManagerTable.NhanVienTable._ID +" = ?", new String[]{ nhanVien.getMa() });
            db.close();
        }catch(Exception ex) {
            Log.e("Error DB", ex.getMessage());
        }
        return result > 0;

    }

    public boolean delNhanVienByMaPB(int maPB){
        boolean result = false;
        String sql_where  =  ManagerTable.NhanVienTable.MAPB+" = "+maPB;

        try {
            db = dbHelper.getWritableDatabase();
            result = db.delete(ManagerTable.NhanVienTable.TABLE_NAME, sql_where,null) > 0;
            db.close();
        }catch(Exception ex) {
            Log.e("Error DB", ex.getMessage());
        }
        return result;
    }
    public boolean delNhanVienByMaNV(int maNV){
        boolean result = false;
        String sql_where  =  ManagerTable.NhanVienTable._ID + " = "+maNV;

        try {
            db = dbHelper.getWritableDatabase();
            result = db.delete(ManagerTable.NhanVienTable.TABLE_NAME, sql_where,null) > 0;
            db.close();
        }catch(Exception ex) {
            Log.e("Error DB", ex.getMessage());
        }
        return result;
    }

}
