package com.manager.employee.database;

import android.provider.BaseColumns;

public class ManagerTable {
    public static final class PhongBanTable implements BaseColumns {
        public static final String TABLE_NAME = "phongban";
        public static final String NAME = "ten_phongban";
        public static final String CREATE_AT = "create_at";
    }

    public static final class NhanVienTable implements BaseColumns {
        public static final String TABLE_NAME = "nhanvien";
        public static final String NAME = "ten_nv";
        public static final String NGAY_SINH = "ngay_sinh";
        public static final String SDT = "sdt";
        public static final String LUONG_CB = "luong_cb";
        public static final String HINH = "hinh";
        public static final String MAPB = "ma_pb";

        public static final String OPTION_MORE = "option_more";

        public static final String CREATE_AT = "create_at";
    }
}

