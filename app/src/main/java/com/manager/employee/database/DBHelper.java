package com.manager.employee.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.manager.employee.Config.DefaulValue;

public class DBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "db_manager1111.db";
    private static final int DATABASE_VERSION = 1;
    private static Context c;

    private static DBHelper instance;

    public static DBHelper getInstance(Context context) {
        if (instance == null) {
            synchronized (DBHelper.class) {
                instance = new DBHelper(context, "manager1111.sqlite", null, 1);
            }
        }
        return instance;
    }

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void QueryData(String sql)
    {
        try {
            SQLiteDatabase database = getWritableDatabase();
            database.execSQL(sql);
        }catch (Exception e){

        }
    }

    public Cursor GetData(String sql)
    {
        try {
            SQLiteDatabase database = getReadableDatabase();
            return database.rawQuery(sql,null);
        }catch (Exception e){
            return null;
        }
    }


    final String SQL_CREATE_TABLE_PHONGBAN = "CREATE TABLE " + ManagerTable.PhongBanTable.TABLE_NAME + " (" +
            ManagerTable.PhongBanTable._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            ManagerTable.PhongBanTable.NAME + "  TEXT,"+
            ManagerTable.PhongBanTable.CREATE_AT + " TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,"
            + "UNIQUE (" + ManagerTable.PhongBanTable.NAME +") ON CONFLICT REPLACE )";

    final String CREATE_DEFAUL_PHONGBAN = "INSERT INTO " + ManagerTable.PhongBanTable.TABLE_NAME + " ( " + ManagerTable.PhongBanTable.NAME + ")" +
            "VALUES" +
            "(\" Kỹ Thuật \")," +
            "(\" Nhân Sự \")," +
            "(\"Kinh Doanh\")";

    final String SQL_CREATE_TABLE_NHANVIEN = "CREATE TABLE " + ManagerTable.NhanVienTable.TABLE_NAME + " (" +
            ManagerTable.NhanVienTable._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            ManagerTable.NhanVienTable.NAME + "  TEXT,"+
            ManagerTable.NhanVienTable.NGAY_SINH + "  TEXT,"+
            ManagerTable.NhanVienTable.SDT + "  TEXT,"+
            ManagerTable.NhanVienTable.LUONG_CB + "  TEXT,"+
            ManagerTable.NhanVienTable.HINH + " BLOB,"+
            ManagerTable.NhanVienTable.OPTION_MORE + " TEXT,"+
            ManagerTable.NhanVienTable.MAPB + " INTEGER,"+
            ManagerTable.NhanVienTable.CREATE_AT + " TIMESTAMP DEFAULT CURRENT_TIMESTAMP )";



    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

//
        try{
            sqLiteDatabase.execSQL(SQL_CREATE_TABLE_PHONGBAN);
            sqLiteDatabase.execSQL(CREATE_DEFAUL_PHONGBAN);
            sqLiteDatabase.execSQL(SQL_CREATE_TABLE_NHANVIEN);

            sqLiteDatabase.execSQL(DefaulValue.CREATE_DEFAUL_NHANVIEN);
        }catch (Exception e){
            Log.d("SQLiteDatabase",e.getMessage());
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        //For User Update
        if(newVersion>oldVersion){
           // sqLiteDatabase.execSQL(SQL_CREATE_TABLE_NHANVIEN);

        }else {

        }
    }
}