package com.manager.employee.View.Activity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.manager.employee.Adapter.PhongBanAdapter;
import com.manager.employee.Interface.IChoose;
import com.manager.employee.Interface.IPhongBanClick;
import com.manager.employee.Interface.InputPhongBan;
import com.manager.employee.Model.PhongBan;
import com.manager.employee.R;
import com.manager.employee.View.Dialog.CustomDialogChoose;
import com.manager.employee.View.Dialog.CustomDialogPhongBan;
import com.manager.employee.database.NhanVienDB;
import com.manager.employee.database.PhongBanDB;

import java.util.ArrayList;

public class PhongBanActivity extends AppCompatActivity {
    RecyclerView rcv_Data;
    ArrayList<PhongBan> arrayList;
    PhongBanAdapter phongBanAdapter;
    Toolbar myToolbar;
    PhongBanDB phongBanDB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phong_ban);
        phongBanDB = new PhongBanDB(PhongBanActivity.this);

        maping();

        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        setUpListData();

        loadDsPhongBan();

    }

    void maping(){
        myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        rcv_Data = findViewById(R.id.rcv_Data);
    }

    String queryCurrent = "";

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu_phongban, menu);
        MenuItem searchItem = menu.findItem(R.id.menuSearch);
        androidx.appcompat.widget.SearchView  searchView = (androidx.appcompat.widget.SearchView) searchItem.getActionView();

        EditText searchEditText = (EditText) searchView.findViewById(R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.white));
        searchEditText.setHintTextColor(getResources().getColor(R.color.white));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                queryCurrent = s+"";
                 phongBanAdapter.getFilter().filter(s);
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.add_phongban:
                CustomDialogPhongBan customDialogPhongBan = new CustomDialogPhongBan();
                customDialogPhongBan.showDialogAddPB(PhongBanActivity.this, new InputPhongBan() {
                    @Override
                    public void inputString(String value) {
                        if(value.length() != 0){
                            phongBanDB.addPhongBan(value);
                            loadDsPhongBan();
                        }
                    }
                });
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;

            default:return super.onOptionsItemSelected(item);
        }

    }

    void setUpListData(){
        arrayList = new ArrayList<>();
        phongBanAdapter = new PhongBanAdapter(arrayList,PhongBanActivity.this);

        phongBanAdapter.setPhongBanClick(new IPhongBanClick() {
            @Override
            public void onLongClick(View view, PhongBan phongBan) {
                PopupMenu popup = new PopupMenu(PhongBanActivity.this, view);
                popup.inflate(R.menu.popup_menu_phongban);

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        int mapb = phongBan.getMaPB();
                        switch (item.getItemId()) {
                            case R.id.menuItem_sua:
                                if(mapb == 1 || mapb == 2 || mapb == 3){
                                    Toast.makeText(PhongBanActivity.this, "Không được sửa Phòng Ban này", Toast.LENGTH_SHORT).show();
                                }else {
                                    CustomDialogPhongBan customDialogPhongBan = new CustomDialogPhongBan();
                                    customDialogPhongBan.showDialogUpdatePB(PhongBanActivity.this, phongBan.getTenPB(),new InputPhongBan() {
                                        @Override
                                        public void inputString(String value) {
                                            if(value.length() != 0){
                                                phongBanDB.updatePhongBan(phongBan.getMaPB(),value);
                                                loadDsPhongBan();
                                            }
                                        }
                                    });
                                }


                                break;
                            case R.id.menuItem_xoa:

                                if(mapb == 1 || mapb == 2 || mapb == 3){
                                    Toast.makeText(PhongBanActivity.this, "Không được xoá Phòng Ban này", Toast.LENGTH_SHORT).show();
                                }else {
                                    CustomDialogChoose customDialogChoose = new CustomDialogChoose();
                                    customDialogChoose.showDialog1(PhongBanActivity.this, "Xoá Phòng Ban", "Bạn có chắc xoá Phòng ban này ?\nSẽ xoá tất cả nhân viên trong Phòng Ban", new IChoose() {
                                        @Override
                                        public void choose(int value) {
                                            if(value == 1){
                                                phongBanDB.deletePhongBan(phongBan.getMaPB());
                                                loadDsPhongBan();
                                                NhanVienDB nhanVienDB = new NhanVienDB(PhongBanActivity.this);
                                                nhanVienDB.delNhanVienByMaPB(phongBan.getMaPB());
                                            }
                                        }
                                    });
                                }

                                break;

                            default:

                                break;
                        }
                        return  true;
                    }
                });

                // Show the PopupMenu.
                popup.show();
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PhongBanActivity.this, LinearLayoutManager.VERTICAL, false);
        rcv_Data.setAdapter(phongBanAdapter);
        rcv_Data.setLayoutManager(linearLayoutManager);
    }


    void loadDsPhongBan(){
        arrayList.clear();
        arrayList.addAll(phongBanDB.getListPhongBan());
        phongBanAdapter.notifyDataSetChanged();
        phongBanAdapter.getFilter().filter(queryCurrent);
    }
}