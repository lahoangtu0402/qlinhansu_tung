package com.manager.employee.View.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.manager.employee.Adapter.PhongBanAdapter;
import com.manager.employee.Adapter.PhongBanFilterAdapter;
import com.manager.employee.Config.Config;
import com.manager.employee.Interface.IChoose;
import com.manager.employee.Interface.IPBCheckedChange;
import com.manager.employee.Interface.IPhongBanClick;
import com.manager.employee.Interface.InputPhongBan;
import com.manager.employee.Model.PhongBan;
import com.manager.employee.R;
import com.manager.employee.View.Dialog.CustomDialogChoose;
import com.manager.employee.View.Dialog.CustomDialogPhongBan;
import com.manager.employee.database.PhongBanDB;

import java.util.ArrayList;

public class FilterActivity extends AppCompatActivity {

    RecyclerView rcv_Data;
    ArrayList<PhongBan> arrayList;
    PhongBanFilterAdapter phongBanAdapter;
    Toolbar myToolbar;
    PhongBanDB phongBanDB;
    CheckBox ckb_allPhongBan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        phongBanDB = new PhongBanDB(FilterActivity.this);

        maping();

        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        setChecked();
        setUpListData();
        loadDsPhongBan();

    }

    boolean checkAll = true;
    void maping(){
        myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        rcv_Data = findViewById(R.id.rcv_Data);

        ckb_allPhongBan = findViewById(R.id.ckb_allPhongBan);


    }
    void setChecked(){
        checkAll = Config.getInstance().getCheckAllFilter(FilterActivity.this);
        ckb_allPhongBan.setChecked(checkAll);
        ckb_allPhongBan.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                checkAll = isChecked;
                Config.getInstance().setCheckAllFilter(FilterActivity.this,isChecked);
                if(checkAll == true){
                    for (int i = 0 ; i < arrayList.size() ; i++){
                        arrayList.get(i).setChecked(true);
                        phongBanAdapter.notifyDataSetChanged();
                    }
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:return super.onOptionsItemSelected(item);
        }

    }

    void setUpListData(){
        arrayList = new ArrayList<>();
        phongBanAdapter = new PhongBanFilterAdapter(arrayList,FilterActivity.this);
        phongBanAdapter.setIpbCheckedChange(new IPBCheckedChange() {
            @Override
            public void valueChange(int index, boolean value) {
                arrayList.get(index).setChecked(value);

                if(value == true){
                    Config.getInstance().putPBChecked(FilterActivity.this,arrayList.get(index).getMaPB());
                }else{
                    checkAll = false;
                    Config.getInstance().setCheckAllFilter(FilterActivity.this,false);
                    ckb_allPhongBan.setChecked(checkAll);

                    Config.getInstance().delPBChecked(FilterActivity.this,arrayList.get(index).getMaPB());
                }
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(FilterActivity.this, LinearLayoutManager.VERTICAL, false);
        rcv_Data.setAdapter(phongBanAdapter);
        rcv_Data.setLayoutManager(linearLayoutManager);

    }


    void loadDsPhongBan(){
        arrayList.clear();
        ArrayList<PhongBan> temp = phongBanDB.getListPhongBan();
        if(checkAll == true){
            for (int i = 0 ; i < temp.size() ; i++){
                temp.get(i).setChecked(true);
            }
        }else{
            for(int i = 0 ; i < temp.size() ; i++){
                int maPB = temp.get(i).getMaPB();
                boolean isCheck = Config.getInstance().getCheckFilter(FilterActivity.this,maPB);
                temp.get(i).setChecked(isCheck);
            }

        }
        arrayList.addAll(temp);
        phongBanAdapter.notifyDataSetChanged();
    }
}