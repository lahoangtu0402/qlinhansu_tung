package com.manager.employee.View.Activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.manager.employee.Adapter.CustomListPBAdapter;
import com.manager.employee.Model.Employee;
import com.manager.employee.Model.PhongBan;
import com.manager.employee.R;
import com.manager.employee.database.PhongBanDB;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class InfoActivity extends AppCompatActivity {
    Toolbar myToolbar;
    PhongBanDB phongBanDB;
    ArrayList<PhongBan> arrayListPhongBan;

    AutoCompleteTextView au_PhongBan;

    CustomListPBAdapter customListPBAdapter;

    TextInputEditText input_more;
    TextInputLayout moreView;

    TextInputEditText input_MSNV;
    TextInputEditText input_NgaySinh;
    LinearLayout ln_NgaySinh;

    TextInputEditText input_SDT;

    TextInputEditText input_LuongCB;
    TextInputEditText input_Luong;

    ImageView img_Camera;
    CircleImageView profile_image;


    int index = -1;
    byte[] hinh ;
    PhongBan phongBanCurrent;
    Employee employeeCurrent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        maping();
        loadDSPhongBan();


        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Intent intent = getIntent();
        Employee employee = (Employee) intent.getSerializableExtra("employee");
        employeeCurrent = employee;
        index = intent.getIntExtra("index",-1);
        mapDataEdit(employee);
    }

    void mapDataEdit(Employee employee){
        myToolbar.setTitle(employee.getTen());
        setPhongBan(employee.getMaPB());
        input_NgaySinh.setText(employee.getNgaySinh());
        input_MSNV.setText("NV - "+employee.getMa());
        input_LuongCB.setText(employee.getLuongCB()+"");
        input_SDT.setText(employee.getSdt());

        input_more.setText(employee.getMoreOption());

        hinh = employee.getHinh();
        if(hinh != null && hinh.length != 0){
            Bitmap bitmap = BitmapFactory.decodeByteArray(hinh, 0, hinh.length);
            profile_image.setImageBitmap(bitmap);
        }

        input_Luong.setText(employee.tinhLuong()+"");

    }

    void setPhongBan(int maPB){
        int indexSelected = -1;
        for(int i = 0 ; i < arrayListPhongBan.size() ; i++){
            if(arrayListPhongBan.get(i).getMaPB() == maPB){
                indexSelected = i;
                break;
            }
        }
        phongBanCurrent = arrayListPhongBan.get(indexSelected);

        au_PhongBan.setListSelection(indexSelected);
        au_PhongBan.setText( arrayListPhongBan.get(indexSelected).getTenPB(),false);
        changeViewOption(arrayListPhongBan.get(indexSelected).getMaPB());
    }
    void changeViewOption(int maPB){
        moreView.setVisibility(View.VISIBLE);
        if(maPB <= 3){
            switch (maPB){
                case 3 ://kinh doanh
                    moreView.setHint("Doanh thu");
                    input_more.setText("0");
                  //  input_more.addTextChangedListener(new MoneyTextWatcher(input_more));
                    break;
                case 2://nhan su
                    moreView.setHint("Số lượng Nhân viên quản lí");
                    input_more.setText("0");
                   // input_more.addTextChangedListener(new MoneyTextWatcher(input_more));
                    break;
                case 1://ky thuat
                    moreView.setHint("Trợ cấp");
                    input_more.setText("0");
//                    input_more.addTextChangedListener(new MoneyTextWatcher(input_more));
                    break;
            }
        }else{
            moreView.setHint("Trợ cấp");
            input_more.setHint("Trợ cấp");
            input_more.setText("5000000");
        }
    }

    void maping(){
        myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        au_PhongBan = findViewById(R.id.au_PhongBan);
        input_more = findViewById(R.id.input_more);
        moreView = findViewById(R.id.moreView);
        moreView.setVisibility(View.GONE);

        input_MSNV = findViewById(R.id.input_MSNV);
        input_NgaySinh = findViewById(R.id.input_NgaySinh);


        input_SDT = findViewById(R.id.input_SDT);
        input_SDT.addTextChangedListener(new PhoneNumberFormattingTextWatcher());

        input_LuongCB = findViewById(R.id.input_LuongCB);
//        input_LuongCB.addTextChangedListener(new MoneyTextWatcher(input_LuongCB));
        input_Luong = findViewById(R.id.input_Luong);
//        input_Luong.addTextChangedListener(new MoneyTextWatcher(input_Luong));

        profile_image = findViewById(R.id.profile_image);

    }
    void loadDSPhongBan(){
        phongBanDB = new PhongBanDB(InfoActivity.this);
        arrayListPhongBan = new ArrayList<>();
        arrayListPhongBan.addAll(phongBanDB.getListPhongBan());
        customListPBAdapter = new CustomListPBAdapter(InfoActivity.this,R.layout.autocompleteitem,arrayListPhongBan);
        au_PhongBan.setAdapter(customListPBAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_item_edit:
                Intent intent = new Intent(InfoActivity.this,EditNhanVienActivity.class);
                intent.putExtra("employee", employeeCurrent);
                intent.putExtra("index",index);
                startActivityForResult(intent,102);
                return true;
            case android.R.id.home:
                Intent returnIntent = new Intent();
                returnIntent.putExtra("employee", employeeCurrent);
                returnIntent.putExtra("index",index);
                setResult(Activity.RESULT_OK,returnIntent);
                finish();
                return true;

            default:return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 102){
            if(resultCode == Activity.RESULT_OK){
                Employee employee = (Employee) data.getSerializableExtra("employee");
                employeeCurrent = employee;
                mapDataEdit(employeeCurrent);
            }

        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu_edit, menu);
        return true;
    }
}