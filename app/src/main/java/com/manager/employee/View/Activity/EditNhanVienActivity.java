package com.manager.employee.View.Activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.manager.employee.Adapter.CustomListPBAdapter;
import com.manager.employee.Model.Employee;
import com.manager.employee.Model.KinhDoanh;
import com.manager.employee.Model.KyThuat;
import com.manager.employee.Model.NVKhac;
import com.manager.employee.Model.NhanSu;
import com.manager.employee.Model.PhongBan;
import com.manager.employee.R;
import com.manager.employee.View.Dialog.CBottomSheetDialog;
import com.manager.employee.database.NhanVienDB;
import com.manager.employee.database.PhongBanDB;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

public class EditNhanVienActivity extends AppCompatActivity {
    Toolbar myToolbar;
    PhongBanDB phongBanDB;
    ArrayList<PhongBan> arrayListPhongBan;

    AutoCompleteTextView au_PhongBan;

    CustomListPBAdapter customListPBAdapter;

    TextInputEditText input_more;
    TextInputLayout moreView;

    TextInputEditText input_MSNV;
    TextInputEditText input_NgaySinh;
    LinearLayout ln_NgaySinh;

    TextInputEditText input_SDT;

    TextInputEditText input_LuongCB;

    ImageView img_Camera;
    CircleImageView profile_image;

    TextInputEditText input_Ten;

    int index = -1;
    byte[] hinh ;
    PhongBan phongBanCurrent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_them_nhan_vien);

        maping();
        loadDSPhongBan();


        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        Intent intent = getIntent();
        Employee employee = (Employee) intent.getSerializableExtra("employee");
        index = intent.getIntExtra("index",-1);
        //index == -1 : edit ; index == -1 : them
        if(index != -1){
            mapDataEdit(employee);
        }else{//them
            myToolbar.setTitle("Thêm Nhân viên ");
            loadMaNV();
        }
    }


    void mapDataEdit(Employee employee){
        myToolbar.setTitle("Sửa Thông tin Nhân viên ");
        setPhongBan(employee.getMaPB());
        input_NgaySinh.setText(employee.getNgaySinh());
        input_MSNV.setText("NV - "+employee.getMa());
        input_LuongCB.setText(employee.getLuongCB()+"");
        input_SDT.setText(employee.getSdt());
        input_Ten.setText(employee.getTen());
        input_more.setText(employee.getMoreOption());

        hinh = employee.getHinh();
        if(hinh != null && hinh.length != 0){
            Bitmap bitmap = BitmapFactory.decodeByteArray(hinh, 0, hinh.length);
            profile_image.setImageBitmap(bitmap);
        }

    }

    void setPhongBan(int maPB){
        int indexSelected = -1;
        for(int i = 0 ; i < arrayListPhongBan.size() ; i++){
            if(arrayListPhongBan.get(i).getMaPB() == maPB){
                indexSelected = i;
                break;
            }
        }
        phongBanCurrent = arrayListPhongBan.get(indexSelected);

        au_PhongBan.setListSelection(indexSelected);
        au_PhongBan.setText( arrayListPhongBan.get(indexSelected).getTenPB(),false);
        changeViewOption(arrayListPhongBan.get(indexSelected).getMaPB());
    }

    void maping(){
        myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        au_PhongBan = findViewById(R.id.au_PhongBan);
        input_more = findViewById(R.id.input_more);
        moreView = findViewById(R.id.moreView);
        moreView.setVisibility(View.GONE);

        input_MSNV = findViewById(R.id.input_MSNV);
        input_NgaySinh = findViewById(R.id.input_NgaySinh);
        ln_NgaySinh = findViewById(R.id.ln_NgaySinh);
        ln_NgaySinh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MaterialDatePicker picker;

                if(input_NgaySinh.getText().toString().length() == 0){
                    picker = MaterialDatePicker.Builder.datePicker()
                            .setTitleText("Chọn ngày")
                            .setSelection(MaterialDatePicker.todayInUtcMilliseconds())
                            .build();
                }else{
                    picker = MaterialDatePicker.Builder.datePicker()
                            .setTitleText("Chọn ngày")
                            .setSelection(convertStringDateToLong(input_NgaySinh.getText().toString(),"dd/MM/yyyy"))
                            .build();
                }

                picker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener() {
                    @Override
                    public void onPositiveButtonClick(Object selection) {
                        long value = (long)selection;
                        String temp = convertDate(value+"","dd/MM/yyyy");
                        input_NgaySinh.setText(temp);
                    }
                });
                picker.show(getSupportFragmentManager(), picker.toString());
            }
        });

        input_SDT = findViewById(R.id.input_SDT);
        input_SDT.addTextChangedListener(new PhoneNumberFormattingTextWatcher());

        input_LuongCB = findViewById(R.id.input_LuongCB);
//        input_LuongCB.addTextChangedListener(new MoneyTextWatcher(input_LuongCB));

        img_Camera = findViewById(R.id.img_Camera);
        profile_image = findViewById(R.id.profile_image);
        img_Camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//
                CBottomSheetDialog bottomSheet = new CBottomSheetDialog(new CBottomSheetDialog.BottomSheetListener() {
                    @Override
                    public void onButtonClicked(boolean isCamera) {
                        if (!isCamera){
                            Intent intent1 = new Intent(Intent.ACTION_GET_CONTENT);
                            intent1.setType("image/*");
                            startActivityForResult(intent1, OPEN_GALLERY);
                        }else{
                            if(ContextCompat.checkSelfPermission(EditNhanVienActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
                                ActivityCompat.requestPermissions(
                                        EditNhanVienActivity.this,
                                        new String[]{Manifest.permission.CAMERA},
                                        REQUEST_CODE_CAMERA
                                );
                            }else{
                                openCamera();
                            }
                        }

                    }
                });
                bottomSheet.show(getSupportFragmentManager(), "exampleBottomSheet");

            }
        });

        input_Ten = findViewById(R.id.input_Ten);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == REQUEST_CODE_CAMERA){
            if(ContextCompat.checkSelfPermission(EditNhanVienActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED){
                openCamera();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    static int REQUEST_CODE_CAMERA = 921;
    static int OPEN_CAMERA = 922;
    static int OPEN_GALLERY = 923;

    void openCamera(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent,OPEN_CAMERA);

    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == OPEN_GALLERY) {

            try {
                Uri uri = data.getData();
                InputStream inputStream = getContentResolver().openInputStream(uri);//biến về
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                profile_image.setImageBitmap(bitmap);

                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 1, bos);
                hinh = bos.toByteArray();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        if (requestCode == OPEN_CAMERA) {

            try {

                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                profile_image.setImageBitmap(bitmap);

                Log.d("resultCode",""+resultCode + "\ndata : "+bitmap.toString());

//
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 1, bos);
                hinh = bos.toByteArray();
                // contactsModeltemp.setImage(bos.toByteArray());

            } catch (Exception e) {
                e.printStackTrace();
                Log.d("Exception",e.getMessage());
            }
        }
    }

    public static String convertDate(String dateInMilliseconds,String dateFormat) {
        return DateFormat.format(dateFormat, Long.parseLong(dateInMilliseconds)).toString();
    }
    public static long convertStringDateToLong(String date, String dateFormat){
        SimpleDateFormat f = new SimpleDateFormat(dateFormat);
        try {
            Date d = f.parse(date);
            long milliseconds = d.getTime();
            return milliseconds + 86400000;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return MaterialDatePicker.todayInUtcMilliseconds();
    }
    void loadDSPhongBan(){
        phongBanDB = new PhongBanDB(EditNhanVienActivity.this);
        arrayListPhongBan = new ArrayList<>();
        arrayListPhongBan.addAll(phongBanDB.getListPhongBan());
        customListPBAdapter = new CustomListPBAdapter(EditNhanVienActivity.this,R.layout.autocompleteitem,arrayListPhongBan);
        au_PhongBan.setAdapter(customListPBAdapter);
        au_PhongBan.setOnItemClickListener(onItemClickListener);

    }

    void loadMaNV(){
        NhanVienDB nhanVienDB = new NhanVienDB(EditNhanVienActivity.this);
        int newIndex = nhanVienDB.getIDNew();
        input_MSNV.setText("NV-"+newIndex+"");
    }



    void changeViewOption(int maPB){
        moreView.setVisibility(View.VISIBLE);
        if(maPB <= 3){
            switch (maPB){
                case 3 ://kinh doanh
                    moreView.setHint("Doanh thu");
                    input_more.setText("0");
//                    input_more.addTextChangedListener(new MoneyTextWatcher(input_more));
                    break;
                case 2://nhan su
                    moreView.setHint("Số lượng Nhân viên quản lí");
                    input_more.setText("0");
//                    input_more.addTextChangedListener(new MoneyTextWatcher(input_more));
                    break;
                case 1://ky thuat
                    moreView.setHint("Trợ cấp");
                    input_more.setText("0");
//                    input_more.addTextChangedListener(new MoneyTextWatcher(input_more));
                    break;
            }
        }else{
            moreView.setHint("Trợ cấp");
            input_more.setHint("Trợ cấp");
            input_more.setText("5000000");
        }
    }

    private AdapterView.OnItemClickListener onItemClickListener =
            new AdapterView.OnItemClickListener(){
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    PhongBan phongBan = arrayListPhongBan.get(i);
                    phongBanCurrent = phongBan;
                    changeViewOption(phongBan.getMaPB());
                }
            };
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.menu_item_done:
                try {
                    themNV();
                } catch (ParseException e) {
                    Toast.makeText(EditNhanVienActivity.this, "Error !", Toast.LENGTH_SHORT).show();
                }

                return  true;
            default:return super.onOptionsItemSelected(item);
        }

    }


    void themNV() throws ParseException {
        NhanVienDB nhanVienDB = new NhanVienDB(EditNhanVienActivity.this);
        String ten = input_Ten.getText().toString();
        String sdt = input_SDT.getText().toString();
        String luongCB = input_LuongCB.getText().toString().replace(" ","").trim();
        String ns = input_NgaySinh.getText().toString();
        String op = input_more.getText().toString();
        String msNV = input_MSNV.getText().toString().replace("NV - ","");
        Employee employee;
        if(phongBanCurrent == null){
            Toast.makeText(EditNhanVienActivity.this, "Vui lòng chọn Phòng Ban", Toast.LENGTH_SHORT).show();
            return;
        }
        int mapb = phongBanCurrent.getMaPB();
        if(hinh == null){
            hinh = "".getBytes();
            Log.d("hinh",hinh.length+"");
        }
        NumberFormat f = NumberFormat.getInstance();
        double luongCB2 = f.parse(luongCB).doubleValue();

        switch (mapb){
            case 1:
                //ky thuat
                employee = new KyThuat(msNV,ten,sdt,ns,hinh,luongCB2,f.parse(op).doubleValue(),mapb);
                break;
            case 2://nhan su
                employee = new NhanSu(msNV,ten,sdt,ns,hinh,luongCB2, Integer.parseInt(op),mapb);
                break;
            case 3://KINH DOANH
                employee = new KinhDoanh(msNV,ten,sdt,ns,hinh,luongCB2,f.parse(op).doubleValue(),mapb);
                break;
            default:
                employee = new NVKhac(msNV,ten,sdt,ns,hinh,Double.parseDouble(luongCB),mapb);
                break;
        }

        boolean isDone = false;
        if(index == -1){
            isDone = nhanVienDB.addNhanVien(employee,phongBanCurrent.getMaPB());
        }else{
            //sua nv
            isDone = nhanVienDB.updateNhanVien(employee);
        }

        if(isDone == true){
            Toast.makeText(EditNhanVienActivity.this, "Done !", Toast.LENGTH_SHORT).show();
            Intent returnIntent = new Intent();
            returnIntent.putExtra("employee", employee);
            returnIntent.putExtra("index",index);
            setResult(Activity.RESULT_OK,returnIntent);
            finish();
        }else{
            Toast.makeText(EditNhanVienActivity.this, "Error !", Toast.LENGTH_SHORT).show();
        }
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu_nv, menu);
        return true;
    }
}