package com.manager.employee.View.Dialog;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import com.manager.employee.Interface.InputPhongBan;
import com.manager.employee.R;

public class CustomDialogPhongBan {
    public void showDialogAddPB(Activity activity, InputPhongBan input){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_phongban);

        EditText input_tenPB = (EditText) dialog.findViewById(R.id.input_tenPB);
        dialog.findViewById(R.id.txt_tieptuc).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input.inputString(input_tenPB.getText().toString());
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.txt_Huy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss(); }
        });

        dialog.show();

    }

    public void showDialogUpdatePB(Activity activity,String nameCurrent ,InputPhongBan input){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_phongban);
        TextView txt_title = dialog.findViewById(R.id.txt_title);
        txt_title.setText("Sửa Phòng Ban");
        EditText input_tenPB = (EditText) dialog.findViewById(R.id.input_tenPB);
        input_tenPB.setText(nameCurrent);
        dialog.findViewById(R.id.txt_tieptuc).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input.inputString(input_tenPB.getText().toString());
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.txt_Huy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss(); }
        });

        dialog.show();

    }
}
