package com.manager.employee.View.Dialog;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import com.manager.employee.Interface.IChoose;
import com.manager.employee.Interface.InputPhongBan;
import com.manager.employee.R;

public class CustomDialogChoose {
    public void showDialog1(Activity activity,String title,String des ,IChoose iChoose){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_yes_no);

        TextView txt_title = dialog.findViewById(R.id.txt_title);
        txt_title.setText(title);

        TextView txt_desription = dialog.findViewById(R.id.txt_desription);
        txt_desription.setText(des);

        dialog.findViewById(R.id.txt_tieptuc).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iChoose.choose(1);
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.txt_Huy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iChoose.choose(0);
                dialog.dismiss();
            }
        });

        dialog.show();

    }
}
