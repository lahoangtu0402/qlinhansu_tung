package com.manager.employee.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.manager.employee.Interface.IPhongBanClick;
import com.manager.employee.Model.PhongBan;
import com.manager.employee.R;

import java.util.ArrayList;

public class PhongBanAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {
    ArrayList<PhongBan> arrayList;
    ArrayList<PhongBan> arrayListFillter;
    Context context;

    public PhongBanAdapter(ArrayList<PhongBan> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
        this.arrayListFillter = arrayList;
    }

    IPhongBanClick phongBanClick;

    public void setPhongBanClick(IPhongBanClick phongBanClick) {
        this.phongBanClick = phongBanClick;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_phongban, parent, false);
        return new PhongBanViewHoder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        PhongBan phongBan = arrayList.get(position);
        if(holder instanceof PhongBanViewHoder){
            ((PhongBanViewHoder)holder).txt_Ten.setText(phongBan.getTenPB());
            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    phongBanClick.onLongClick(v,phongBan);
                    return true;
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                FilterResults filterResults = new FilterResults();
                //charSequence -->giá trị nhập vào
                if (charSequence == null || charSequence.length() == 0) {
                    filterResults.values = arrayListFillter;//add nguyên bộ dữ liệu vào
                    filterResults.count = arrayListFillter.size();
                } else {
                    ArrayList<PhongBan> Arrtemp = new ArrayList<>();
                    for (PhongBan phongBan : arrayListFillter) {
                        if ( phongBan.getTenPB().toLowerCase().contains(charSequence.toString().toLowerCase()) ) //chuyển hết về chữ thường
                        {
                            Arrtemp.add(phongBan);
                        }
                    }
                    filterResults.values = Arrtemp;
                    filterResults.count = Arrtemp.size();
                }

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                arrayList = (ArrayList<PhongBan>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    class  PhongBanViewHoder extends  RecyclerView.ViewHolder{
        TextView txt_Ten;
        public PhongBanViewHoder(@NonNull View itemView) {
            super(itemView);
            txt_Ten = itemView.findViewById(R.id.txt_Ten);
        }
    }
}
