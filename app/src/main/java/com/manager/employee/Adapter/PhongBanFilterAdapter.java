package com.manager.employee.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.manager.employee.Interface.IPBCheckedChange;
import com.manager.employee.Interface.IPhongBanClick;
import com.manager.employee.Model.PhongBan;
import com.manager.employee.R;

import java.util.ArrayList;

public class PhongBanFilterAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<PhongBan> arrayList;
    Context context;

    public PhongBanFilterAdapter(ArrayList<PhongBan> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    IPBCheckedChange ipbCheckedChange;


    public void setIpbCheckedChange(IPBCheckedChange ipbCheckedChange) {
        this.ipbCheckedChange = ipbCheckedChange;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_phongban_filter, parent, false);
        return new PhongBanViewHoder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        PhongBan phongBan = arrayList.get(position);
        int index = position;
        if(holder instanceof PhongBanViewHoder){
            ((PhongBanViewHoder)holder).txt_Ten.setText(phongBan.getTenPB());
            ((PhongBanViewHoder)holder).checkBox.setChecked(phongBan.isChecked());
            ((PhongBanViewHoder)holder).checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    ipbCheckedChange.valueChange(index,isChecked);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class  PhongBanViewHoder extends  RecyclerView.ViewHolder{
        TextView txt_Ten;
        CheckBox checkBox;
        public PhongBanViewHoder(@NonNull View itemView) {
            super(itemView);
            txt_Ten = itemView.findViewById(R.id.txt_Ten);
            checkBox = itemView.findViewById(R.id.checkBox);

        }
    }
}
