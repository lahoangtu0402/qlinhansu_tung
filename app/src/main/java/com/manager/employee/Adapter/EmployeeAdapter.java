package com.manager.employee.Adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.manager.employee.Interface.IEmployeeClick;
import com.manager.employee.Model.Employee;
import com.manager.employee.R;
import com.manager.employee.database.PhongBanDB;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class EmployeeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    Activity activity;
    ArrayList<Employee> arrayList;
    ArrayList<Employee> arrayListFillter;

    IEmployeeClick employeeClick;
    PhongBanDB phongBanDB;

    public void setEmployeeClick(IEmployeeClick employeeClick) {
        this.employeeClick = employeeClick;
    }

    public EmployeeAdapter(Activity activity, ArrayList<Employee> arrayList) {
        this.activity = activity;
        this.arrayList = arrayList;
        this.arrayListFillter = arrayList;
        phongBanDB = new PhongBanDB(activity);
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == VIEW_TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_employee, parent, false);
            return new EmployeeViewHoder(v);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if(holder instanceof  EmployeeViewHoder){
            Employee employee = arrayList.get(position);
            int index = position;
           ((EmployeeViewHoder) holder).txt_Ten.setText(employee.getTen());
           if(employee.getMa().contains("NV")){
               ((EmployeeViewHoder) holder).txt_MaNV.setText(""+employee.getMa());
           }else{
               ((EmployeeViewHoder) holder).txt_MaNV.setText("NV-"+employee.getMa());
           }

            ((EmployeeViewHoder) holder).txt_PhongBan.setText(phongBanDB.getTenPB(employee.getMaPB()));

            try{
                if(employee.getHinh() != null && employee.getHinh().length != 0){
                    Bitmap bitmap = BitmapFactory.decodeByteArray(employee.getHinh(), 0, employee.getHinh().length);
                    ((EmployeeViewHoder) holder).profile_image.setImageBitmap(bitmap);
                }else {
                    ((EmployeeViewHoder) holder).profile_image.setImageDrawable(activity.getResources().getDrawable(R.drawable.user));
                }
            }catch (Exception e){
                ((EmployeeViewHoder) holder).profile_image.setImageDrawable(activity.getResources().getDrawable(R.drawable.user));
            }

            ((EmployeeViewHoder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    employeeClick.onClick(v,index,employee);
                }
            });
            ((EmployeeViewHoder) holder).itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    employeeClick.onLongClick(v,index,employee);
                    return true;
                }
            });
        }
        else if (holder instanceof LoadingViewHolder) {
            showLoadingView((LoadingViewHolder) holder, position);
        }
    }
    @Override
    public int getItemViewType(int position) {
        return arrayList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        //return arrayList.size();
        return arrayList == null ? 0 : arrayList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                FilterResults filterResults = new FilterResults();
                //charSequence -->giá trị nhập vào
                if (charSequence == null || charSequence.length() == 0) {
                    filterResults.values = arrayListFillter;//add nguyên bộ dữ liệu vào
                    filterResults.count = arrayListFillter.size();
                } else {
                    ArrayList<Employee> Arrtemp = new ArrayList<>();
                    for (Employee employee : arrayListFillter) {
                        if ( employee.getTen().toLowerCase().contains(charSequence.toString().toLowerCase()))
                        {
                            Arrtemp.add(employee);
                        }
                    }
                    filterResults.values = Arrtemp;
                    filterResults.count = Arrtemp.size();
                }

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                arrayList = (ArrayList<Employee>) filterResults.values;//values --> trả về oject
                notifyDataSetChanged();
            }
        };
    }


    public class EmployeeViewHoder extends RecyclerView.ViewHolder{
        TextView txt_Ten,txt_PhongBan,txt_MaNV;
        CardView container;
        CircleImageView profile_image;
        public EmployeeViewHoder(View itemView) {
            super(itemView);
            // txt_Title = itemView.findViewById(R.id.txt_Title);
            container = itemView.findViewById(R.id.container);
            txt_Ten = itemView.findViewById(R.id.txt_Ten);
            txt_MaNV = itemView.findViewById(R.id.txt_MaNV);
            txt_PhongBan = itemView.findViewById(R.id.txt_PhongBan);
            profile_image = itemView.findViewById(R.id.profile_image);



        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }
    private void showLoadingView(LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed

    }
}
