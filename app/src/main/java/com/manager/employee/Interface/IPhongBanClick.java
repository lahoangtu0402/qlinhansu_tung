package com.manager.employee.Interface;

import android.view.View;

import com.manager.employee.Model.Employee;
import com.manager.employee.Model.PhongBan;

public interface IPhongBanClick {
    void onLongClick(View view, PhongBan phongBan);
}
