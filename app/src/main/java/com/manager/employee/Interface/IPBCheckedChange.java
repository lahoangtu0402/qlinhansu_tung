package com.manager.employee.Interface;

public interface IPBCheckedChange {
    void valueChange(int index, boolean value);
}
