package com.manager.employee.Interface;

import android.view.View;

import com.manager.employee.Model.Employee;

public interface IEmployeeClick {
    void onClick(View view, int index ,Employee employee);
    void onLongClick(View view, int index ,Employee employee);
}
