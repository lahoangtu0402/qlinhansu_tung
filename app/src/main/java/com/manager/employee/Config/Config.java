package com.manager.employee.Config;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.Map;

public class Config {
    private static Config instance;
    public static Config getInstance(){
        if(instance == null){
            synchronized (Config.class){
                instance = new Config();
            }
        }
        return instance;
    }
    private Config() {
    }

    public static int LIMIT_RECORD = 10;

    public void setCheckAllFilter(Context context, boolean isShow ){
        String MY_PREFS_NAME = "CheckAllFilter";
        SharedPreferences.Editor prefsEditor = context.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE).edit();
        prefsEditor.putBoolean("CheckAllFilter",isShow);
        prefsEditor.apply();

    }
    public boolean getCheckAllFilter(Context context){
        String MY_PREFS_NAME = "CheckAllFilter";
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        return prefs.getBoolean("CheckAllFilter",true);
    }

    public void putPBChecked(Context context, int key){
        String MY_PREFS_NAME = "DSFilter";
        SharedPreferences.Editor prefsEditor = context.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE).edit();
        prefsEditor.putBoolean(String.valueOf(key),true);
        prefsEditor.apply();
    }
    public void delPBChecked(Context context, int key){
        String MY_PREFS_NAME = "DSFilter";
        SharedPreferences.Editor prefsEditor = context.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE).edit();
        prefsEditor.remove(String.valueOf(key));
        prefsEditor.apply();
    }
    public boolean getCheckFilter(Context context, int key){
        String MY_PREFS_NAME = "DSFilter";
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        return prefs.getBoolean(String.valueOf(key),false);
    }
    public ArrayList<String> getListFilter(Context context){
        ArrayList<String> arrayList = new ArrayList<>();
        String MY_PREFS_NAME = "DSFilter";
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        Map<String,?> keys = prefs.getAll();
        for(Map.Entry<String,?> entry : keys.entrySet()){
            arrayList.add(entry.getKey()+"");
        }
        return arrayList;
    }


}
