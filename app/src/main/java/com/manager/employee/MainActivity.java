package com.manager.employee;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.manager.employee.Adapter.EmployeeAdapter;
import com.manager.employee.Config.Config;
import com.manager.employee.Interface.IChoose;
import com.manager.employee.Interface.IEmployeeClick;
import com.manager.employee.Model.Employee;
import com.manager.employee.View.Activity.EditNhanVienActivity;
import com.manager.employee.View.Activity.FilterActivity;
import com.manager.employee.View.Activity.InfoActivity;
import com.manager.employee.View.Activity.PhongBanActivity;
import com.manager.employee.View.Dialog.CustomDialogChoose;
import com.manager.employee.database.DBHelper;
import com.manager.employee.database.NhanVienDB;
import com.manager.employee.database.PhongBanDB;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private static MainActivity activity;
    public static MainActivity getInstance(){
        if(activity == null){
            activity = new MainActivity();
        }
        return activity;
    }

    RecyclerView rcv_Data;
    ArrayList<Employee> arrayList;
    EmployeeAdapter employeeAdapter;
    Toolbar myToolbar;

    DBHelper dbHelper;
    PhongBanDB phongBanDB;
    NhanVienDB nhanVienDB;

    boolean isLoading = false;

    static  int REQUEST_CODE_INFO = 187;
    static int REQUEST_CODE_EDIT = 188;
    static  int REQUEST_CODE_ADD = 189;
    static  int REQUEST_CODE_FILTER = 190;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dbHelper = DBHelper.getInstance(MainActivity.this);
        phongBanDB = new PhongBanDB(MainActivity.this);
        nhanVienDB = new NhanVienDB(MainActivity.this);
        phongBanDB.getListPhongBan();
        maping();

        setSupportActionBar(myToolbar);
        setUpListData();
        loadData();

        initScrollListener();
    }
    void maping(){
        myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        rcv_Data = findViewById(R.id.rcv_Data);
    }

    void setUpListData(){
        arrayList = new ArrayList<>();
        employeeAdapter = new EmployeeAdapter(MainActivity.this,arrayList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);
        rcv_Data.setAdapter(employeeAdapter);
        rcv_Data.setLayoutManager(linearLayoutManager);
        employeeAdapter.setEmployeeClick(new IEmployeeClick() {
            @Override
            public void onClick(View view, int index, Employee employee) {
                Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                intent.putExtra("employee", employee);
                intent.putExtra("index",index);
                startActivityForResult(intent,REQUEST_CODE_INFO);

            }

            @Override
            public void onLongClick(View view, int index, Employee employee) {
                showPopup(view,index,employee);
            }
        });

    }

    void showPopup(View view,int index, Employee employee){
        PopupMenu popup = new PopupMenu(MainActivity.this, view, Gravity.LEFT);
        popup.inflate(R.menu.popup_menu_nv);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menuItem_sua:

                        editNV(index,employee);

                        break;
                    case R.id.menuItem_xoa:
                        CustomDialogChoose customDialogChoose = new CustomDialogChoose();
                        customDialogChoose.showDialog1(MainActivity.this, "Xoá Nhân viên", "Bạn có chắc xoá Nhân viên này ? ", new IChoose() {
                            @Override
                            public void choose(int value) {
                                if(value == 1){
                                    NhanVienDB nhanVienDB = new NhanVienDB(MainActivity.this);
                                    nhanVienDB.delNhanVienByMaNV(Integer.parseInt(employee.getMa()));
                                    Toast.makeText(MainActivity.this, "Xoá thành công", Toast.LENGTH_SHORT).show();
                                    loadData();
                                }
                            }
                        });

                        break;

                    default:

                        break;
                }
                return  true;
            }
        });

        // Show the PopupMenu.
        popup.show();
    }


    void editNV(int index, Employee employee){
        Intent intent = new Intent(MainActivity.this,EditNhanVienActivity.class);
        intent.putExtra("employee", employee);
        intent.putExtra("index",index);
        startActivityForResult(intent, REQUEST_CODE_EDIT);
    }

    private void initScrollListener() {
        rcv_Data.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == arrayList.size() - 1) {

                        loadMore();
                        isLoading = true;
                    }
                }
            }
        });
    }

    private void loadMore() {
        arrayList.add(null);
        employeeAdapter.notifyItemInserted(arrayList.size() - 1);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                arrayList.remove(arrayList.size() - 1);
                int scrollPosition = arrayList.size();
                employeeAdapter.notifyItemRemoved(scrollPosition);
                employeeAdapter.notifyDataSetChanged();

                loadMoreData();

                employeeAdapter.notifyDataSetChanged();
                isLoading = false;
            }
        }, 2000);
    }

    boolean isFirst = true;
    void loadMoreData(){

        boolean checkAllPB = Config.getInstance().getCheckAllFilter(MainActivity.this);
        if(checkAllPB){
            int intOffset = arrayList.size();
            if(isFirst != true){
                intOffset = intOffset - 1;
                isFirst = false;
            }
            ArrayList<Employee> arrayListTemp = nhanVienDB.getListNhanVien( intOffset );
            arrayList.addAll(arrayListTemp);

        }else{
            //get list PB checked;
            ArrayList<String> list =  Config.getInstance().getListFilter(MainActivity.this);
            int intOffset = arrayList.size();
            if(isFirst != true){
                intOffset = intOffset - 1;
                isFirst = false;
            }
            arrayList.addAll(nhanVienDB.getListNhanVienByMaPB(list,intOffset));
        }
    }

    void loadData(){
        arrayList.clear();
        boolean checkAllPB = Config.getInstance().getCheckAllFilter(MainActivity.this);
        if(checkAllPB){
            arrayList.addAll(nhanVienDB.getListNhanVien(0));
        }else{
            //get list PB checked;
           ArrayList<String> list =  Config.getInstance().getListFilter(MainActivity.this);
            arrayList.addAll(nhanVienDB.getListNhanVienByMaPB(list,0));
        }

        employeeAdapter.notifyDataSetChanged();

        employeeAdapter.getFilter().filter(queryCurrent);
    }
    String queryCurrent = "";
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu_main, menu);
        MenuItem searchItem = menu.findItem(R.id.menuSearch);
        androidx.appcompat.widget.SearchView  searchView = (androidx.appcompat.widget.SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                queryCurrent = s+"";
                employeeAdapter.getFilter().filter(s);
                return false;
            }
        });
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuSearch:

                return true;
            case R.id.menuFilter:
                Intent intent = new Intent(MainActivity.this, FilterActivity.class);
                startActivityForResult(intent,REQUEST_CODE_FILTER);
                return true;
            case R.id.signout :
                finish();
                return true;
            case R.id.menuPhongBan:
                Intent intentPB = new Intent(MainActivity.this, PhongBanActivity.class);
                startActivity(intentPB);
                return true;
            case R.id.menuAddNV:
                Intent intentAddNV = new Intent(MainActivity.this, EditNhanVienActivity.class);
                startActivityForResult(intentAddNV, REQUEST_CODE_ADD);
                //startActivity(intentAddNV);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if(requestCode == REQUEST_CODE_ADD){
            if(resultCode == Activity.RESULT_OK){
                Employee employee = (Employee) data.getSerializableExtra("employee");
                int maPB = employee.getMaPB();
                boolean checkAllPB = Config.getInstance().getCheckAllFilter(MainActivity.this);
                if(checkAllPB){
                    arrayList.add(0,employee);
                }else{
                    //get list PB checked;
                    ArrayList<String> list =  Config.getInstance().getListFilter(MainActivity.this);
                    if(list.contains(maPB+"")){
                        arrayList.add(0,employee);
                    }
                }
                employeeAdapter.notifyDataSetChanged();
            }

        }

        if(requestCode == REQUEST_CODE_EDIT){
            if(resultCode == Activity.RESULT_OK){
                Employee employee = (Employee) data.getSerializableExtra("employee");
                int index = data.getIntExtra("index",-1);
                if(index != -1){
                    arrayList.set(index,employee);
                    employeeAdapter.notifyDataSetChanged();
                }
            }
        }

        if(requestCode == REQUEST_CODE_INFO){
            if(resultCode == Activity.RESULT_OK){
                Employee employee = (Employee) data.getSerializableExtra("employee");
                int index = data.getIntExtra("index",-1);
                if(index != -1){
                    arrayList.set(index,employee);
                    employeeAdapter.notifyDataSetChanged();
                }
            }
        }

        if(requestCode == REQUEST_CODE_FILTER){
            isFirst = true;
            loadData();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}